<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf

        <label>Full Name:</label><br>
        <input type="text" name="fname"><br><br>

        <label>Last Name:</label><br>
        <input type="text" name="lname"><br><br>

        <label>Gender:</label><br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br>
        <input type="radio" name="gender" value="other">Other<br><br>

        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="malasysian">Malasysian</option>
            <option value="singaporean">Singaporean</option>
            <option value="australian">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="english">English <br>
        <input type="checkbox" name="other">Other <br><br>

        <label>Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>