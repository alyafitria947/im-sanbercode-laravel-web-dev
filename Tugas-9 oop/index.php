<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("Shaun");

echo "Name : $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "Cold Blooded : $sheep->cold_blooded <br>";

$kodok = new frog("Buduk");

echo "<br> Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
$kodok->jump();

$sungokong = new ape("Kera Sakti");

echo "<br> Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold Blooded : $sungokong->cold_blooded <br>";
$sungokong->yell();

?>