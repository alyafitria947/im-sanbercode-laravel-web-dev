<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class GameController extends Controller
{
    public function erd(){
        return view('layouts.erd');
    }

    public function create(){
        return view('layouts.tambah');
    }

    public function store(Request $request){

        $request->validate([
            'name'=>'required',
            'gameplay'=>'required',
            'developer'=>'required',
            'year'=>'required',
        ]);

        DB::table('game')->insert([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year'],
        ]);
        return redirect('/game');
    }

    public function index(){
        $game = DB::table('game')->get();
        
        return view('layouts.tampil', ['game' => $game] );
    }

    public function show($id){
        $game = DB::table('game')->where('id', $id)->first();
        $platform = DB::table('platform')->where('id', $id)->first();

        return view('layouts.detail', ['game' => $game, 'platform' => $platform]);
    }

    public function edit($id){
        $game = DB::table('game')->where('id', $id)->first();
        
        return view('layouts.edit', ['game' => $game]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'name'=>'required',
            'gameplay'=>'required',
            'developer'=>'required',
            'year'=>'required',
        ]);

        $game = DB::table('game')
                    ->where('id', $id)
                    ->update([
                        'name'=>$request->name,
                        'gameplay'=>$request->gameplay,
                        'developer'=>$request->developer,
                        'year'=>$request->year]);
        
        return redirect('/game');
    }

    public function destroy($id){
        DB::table('game')->where('id', $id)->delete();

        return redirect('/game');
    }
}
