@extends('master')

@section('judul')
    Detail Game
@endsection

@section('content')
    <h1>{{ $game->name }} ({{ $game->year }})</h1>
    <p>Developer : {{ $game->developer }}</p>
    <h4>Gameplay</h4>
    <p>{{ $game->gameplay }}</p>
    <h4>Platform</h4>
    <p>{{ $platform->name }}</p>

    <a href="/game" class="btn btn-secondary btn-sm">Kembali</a>
@endsection