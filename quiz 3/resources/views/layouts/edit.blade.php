@extends('master')

@section('judul')
    Edit Game
@endsection

@section('content')
    <form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method('PUT')
        <div class="form-group">
            <label>Nama Game</label>
            <input type="text" name="name" class="form-control" value="{{ $game->name }}">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Gameplay</label>
            <textarea cols="30" rows="10" name="gameplay" class="form-control">{{ $game->gameplay }}</textarea>
        </div>
        @error('gameplay')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Developer</label>
            <input type="text" name="developer" class="form-control" value="{{ $game->developer }}">
        </div>
        @error('developer')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Year</label>
            <input type="text" name="year" class="form-control" value="{{ $game->year }}">
        </div>
        @error('year')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection