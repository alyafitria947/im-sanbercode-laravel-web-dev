@extends('master')

@section('judul')
    List Game
@endsection

@section('content')
    <a href="/game/create" class="btn btn-primary btn-sm">Tambah</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Developer</th>
            <th scope="col-3">Year</th>
        </tr>
        </thead>

        @forelse ($game as $key => $item)
            <tr>
                <td>{{ $key + 1}}</td>
                <td>{{ $item->name}}</td>
                <td>{{ $item->developer}}</td>
                <td>{{ $item->year}}</td>
                <td>
                    <form action="/game/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/game/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
                        <a href="/game/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak Ada Data</td>
            </tr>
        @endforelse 
    </table>

    <script>
        Swal.fire({
          title: "Berhasil!",
          text: "Memasangkan script sweet alert",
          icon: "success",
          confirmButtonText: "Cool",
        });
      </script>
      
@endsection