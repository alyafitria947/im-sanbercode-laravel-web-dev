<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// cek branch

Route::get('/', [GameController::class, 'erd']);

Route::get('/game/create', [GameController::class, 'create']);
Route::post('/game', [GameController::class, 'store']);

Route::get('/game', [GameController::class, 'index']);
Route::get('/game/{game_id}', [GameController::class, 'show']);

Route::get('/game/{game_id}/edit', [GameController::class, 'edit']);
Route::put('/game/{game_id}', [GameController::class, 'update']);

Route::delete('/game/{game_id}', [GameController::class, 'destroy']);
